(function (window,undefined) {
    'use strict';

    if( ! window.renderTemplates ){

        var init  = {


            templateRender: function( params, callback ){

                var data = params.parse ? JSON.parse( params.data ) : params.data;
                $.get( params.view, function (template) {
                    var rendered = Mustache.render(template, data);
                    callback(false, rendered );
                });

            }

            ,render: function( params , callback ){

                if( params.url ){

                    $.ajax({
                        url: params.url,
                        type: "GET",
                        dataType: "json",
                        success: function (xhr) {
                            init.templateRender( Object.assign({data: xhr.msg}, params ), callback )
                        },
                        error: function (xhr) {
                            callback(xhr, false);
                        }
                    });

                }else{
                    this.templateRender( params, callback )
                }

            }


        }

        window.renderTemplates = init;
    }


})(window);
