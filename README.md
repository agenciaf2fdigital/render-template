#Render template

> Vamos tentar criar um padrão



``````
<script src="renderTemplates.js" type="text/javascript" charset="utf-8"></script>

<script>
renderTemplates.render( rest_url, view_url , function(err, rendered){
    $(body).html( rendered );
})
</script>
``````


Bower
``````

bower install 'git@bitbucket.org:agenciaf2fdigital/render-template.git#deploy'


``````

Produção branch Deploy


Dependencias atuais:

* [jQuery](https://jquery.com/)
* [Mustache](https://github.com/janl/mustache.js)


Melhorias:

* Tirar do jQuery
* Colocar o [Handlebars](http://handlebarsjs.com/)
* Criar modulo para partials
* Preparar para [Requirejs](http://requirejs.org/)
